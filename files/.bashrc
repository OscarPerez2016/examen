# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias ls='ls --color=auto'
 
LS_COLORS="no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01"
LS_COLORS="$LS_COLORS:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42"
LS_COLORS="$LS_COLORS:ow=34;42:st=37;44:ex=01;32"
# Archivos comprimidos
LS_COLORS="$LS_COLORS:*.arj=0;91:*.bz2=0;91:*.gz=0;91:*.jar=01;31:*.lzh=0;91"
LS_COLORS="$LS_COLORS::*.tar=0;91:*.taz=0;91:*.tgz=0;91:*.zip=0;91:*.z=0;91:*.Z=0;91"
# Paquetes deb, rpm y pacman
LS_COLORS="$LS_COLORS:*.deb=01;31:*.rpm=01;31:*.pkg.tar.xz=01;31"
# Imagenes
LS_COLORS="$LS_COLORS:*.bmp=0;35:*.gif=0;35:*.jpg=0;35:*.jpeg=0;35:*.pbm=0;35"
LS_COLORS="$LS_COLORS:*.pgm=0;35:*.png=0;35:*.ppm=0;35:*.svg=0;35:*.tga=0;35"
LS_COLORS="$LS_COLORS:*.tif=0;35:*.tiff=0;35:*.xbm=0;35:*.xpm=0;35"
# Videos
LS_COLORS="$LS_COLORS:*.avi=0;35:*.dl=0;35:*.fli=0;35:*.gl=0;35:*.mov=0;35"
LS_COLORS="$LS_COLORS:*.mpg=0;35:*.mpeg=0;35:*.xcf=0;95:*.xwd=0;95"
# Audio
LS_COLORS="$LS_COLORS:*.flac=0;36:*.mp3=0;36:*.mpc=0;95:*.ogg=0;36:*.wav=0;36"
# Logs
LS_COLORS="$LS_COLORS:*.log=0;33"
 
export LS_COLORS